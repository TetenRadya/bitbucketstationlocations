﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//default system
using System.Threading.Tasks;

namespace Pegawai.Web.Models //directory tempat penyimpan class ada di Pegawai.web/models
{
    public class KaryawanViewModel
    {
        // defaulnya kosong
        public string NIP { get; set; }
        public string NamaPegawai { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public DateTime TanggalLahir { get; set; }
        //public = di publicasikan ke project, datatime = type colom table, tanggallahir=nama kolom table


    }

}