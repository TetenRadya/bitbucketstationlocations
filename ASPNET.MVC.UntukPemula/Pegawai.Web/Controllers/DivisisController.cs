﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Pegawai.Web.Models;

namespace Pegawai.Web.Controllers //ini nama sebuah controller 
{
    public class DivisisController : Controller // ini adalah sebuah controller yang akan di tampilkan di sebuah form divisi, jika dalam vb.net di sebut divs\isi.vb
    {
        private PegawaiDBEntities db = new PegawaiDBEntities();
        // di atas adalah sebuah conection database di pemograman jika di vb kita suka panggail dengan call konek() / memanggail str konekis
        // dalam contoh disini str nya adalah PegawaiDBEntitie

        // GET: Divisis
        [Authorize(Roles = "Admin, Pimpinan")]
        public ActionResult Index() // di vb biasa di sub event sub, saya biasa di vb dengan memanggil sub tampil()/ sub mulai()
        {
            return View(db.Divisis.ToList()); 
            //sintak ini adala perintah untuk menampilkan (View) database/db PegawaiDBEntitie dengan table bernama divisi
        }

        // GET: Divisis/Details/5
        [Authorize(Roles = "Admin, Pimpinan")]
        public ActionResult Details(int? id)
        //aksi ini menampiklan event/tombol detail dalam vb saya biasa gunakan sub tampil()
        {
            if (id == null) //jika id tidak ada maka akan muncul error "bad request"
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Divisi divisi = db.Divisis.Find(id); 
            //jika di temukan maka akan memanggil table divisi dari database jika id yang di cari tidak ada maka akan ada info form tidak ada
            if (divisi == null)
            {
                return HttpNotFound();
            }
            return View(divisi);//table divisi
            //jika di temukan maka akan di tampilkan /divisi/details/id
        }

        // GET: Divisis/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        //ketika mengklik tombol create maka akan muncul form create
        {
            return View();
        }

        // POST: Divisis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "DivisiID,NamaDivisi")] Divisi divisi)
        //ini adalah event simpan jika di vb 
        //"Private Sub bsimpan_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bsimpan.Click"
        {
            if (ModelState.IsValid)
            //setelah kita input di form dan apa yang di inginkan oleh model yang kita buat misal ada format tanggal yyyy/mm/dd 
            //terus kita input format tanggal dengan mm/dd/yyyy maka model state akan menginformasika ada kesalahan atau error dan 
            // akan form akan tetap di form create "return View(divisi);" sampai inputan sesuai yang di ingin kan oleh model

            {
                
                db.Divisis.Add(divisi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(divisi);
        }

        // GET: Divisis/Edit/5
        [Authorize(Roles = "Admin, Pimpinan")]
        public ActionResult Edit(int? id)//peritah edit sama dengan pertintah detail, cuma saja perbedaannya di form / view
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Divisi divisi = db.Divisis.Find(id);
            if (divisi == null)
            {
                return HttpNotFound();
            }
            return View(divisi);
        }

        // POST: Divisis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Pimpinan")]
        public ActionResult Edit([Bind(Include = "DivisiID,NamaDivisi")] Divisi divisi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(divisi).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(divisi);
            
        }

        // GET: Divisis/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Divisi divisi = db.Divisis.Find(id);
            if (divisi == null)
            {
                return HttpNotFound();
            }
            return View(divisi);
        }

        // POST: Divisis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Divisi divisi = db.Divisis.Find(id);
            db.Divisis.Remove(divisi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
