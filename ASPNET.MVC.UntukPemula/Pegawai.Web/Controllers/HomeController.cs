﻿using Pegawai.Web.Models;//digunakan untuk kebutuhan view yang melibatkan model
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pegawai.Web.Controllers //tempat nyimpan controller
{
    public class HomeController : Controller 
        //nama controller home controller akan otomatis membentuk folder home di folder views
    {
        // GET: Home
        public ActionResult Index()
            //url akan mememanggil nama controller bernama index controler 
            //dan menampilkan view yang bernama index di directory nama controller akan terjadi error jika index view tidak di temukan
        {
            return View();
        }
        // Get Login

        public ActionResult Login()
        {
            return View();

        }
        //post login
        [HttpPost]
        public ActionResult Login(LoginViewModel data)
            //ini ada error di loginviewmodel errornya apa? 
            //ternyata membutuhkan configrasi model yang bernama login view model yang berada di folder model
        {
            return View();
        }

        // Get PageNotFound
        public ActionResult PageNotFound()
        {
            return View();
        }

    }
}