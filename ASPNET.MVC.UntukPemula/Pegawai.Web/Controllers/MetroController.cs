﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pegawai.Web.Controllers
{
    public class MetroController : Controller
    {
        // GET: Metro
        
        public ActionResult Index()
        {
            ViewBag.Title = "Cing";
            return View();
        }
        // Get : User interface
        public ActionResult UI()
        {
            return View();
        }
        // Get : form
        public ActionResult Forms()
        {
            return View();
        }

        // Get : gambar
        public ActionResult Charts()
        {
            return View();
        }

        // Get : tabel
        public ActionResult Tables()
        {
            return View();
        }
        // Get : Latihan
        public ActionResult Latihan()
        {
            return View();
        }

        //post latihan
        [HttpPost]
        public ActionResult Latihan(string TextBoxNama, string TextAreaAlamat, string DropdownlistPropinsi, string RadioButtonKelamin, bool CeckBCSharp,
            bool CeckBJava, bool CeckBPhyton, IEnumerable<string> listBoxPekerjaan)
        {
            string languages = string.Empty;
            string jobs = string.Empty;
            if (CeckBCSharp)
            {
                languages = "C#" + "" + languages;

            }

            if (CeckBJava)
            {
                languages = "JAVA" + "" + languages;
            }
            if (CeckBPhyton)
            {
                languages = "PHYTON" + "" + languages;
            }
            foreach (string job in listBoxPekerjaan)
            {
                if (job.Equals("1"))
                {
                    jobs = "Programmer " + jobs;
                }
                else if
                      (job.Equals("2"))
                {
                    jobs = "Analis Sistem " + jobs;
                }
                else if
                     (job.Equals("3"))
                {
                    jobs = "Manager Projek " + jobs;
                }
                else
                {
                    jobs = "Programmer " + jobs;
                }
            }

            ViewBag.Hasil = "Nama = " + TextBoxNama + " , Alamat = " + TextAreaAlamat + " , Propinsi = " + DropdownlistPropinsi + " , Jenis kelamin = " +
                RadioButtonKelamin + " , Bahasa Pemograman = " + languages + " , Pengalaman Kerja = " + jobs;

            return View();
        }


        public ActionResult Tes()
        {
            return View();
        }
        public ActionResult Tos()
        {
            return View();
        }
    }
}