﻿using ASP.EMPTY.MVC.Models; //memanggil model
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP.EMPTY.MVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home -> dalam laravel ini tersimpan dalam routes.php dimana fungsi get dan post
        public ActionResult Index() //ngambil view di index dalam laravel "public function Index()" 
        {
            return View();
        }
        [HttpPost] //fungsi post untuk ketika mengklik tombol button
        public ActionResult Index(visitor coy) // ngambil view dari index dan data dari model 
        {
            ViewBag.pesan = "hello " + coy.NAMA + " di " + coy.ALAMAT;
                return View();
        }

        public ActionResult latihan()
        {
            return View();
        }
    }
}